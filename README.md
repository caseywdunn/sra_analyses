# An SRA summary tool

To execute the analysis in R, run:

    library(knitr)
    knit2html("sra_summary.rmd")

This produces output files in two different formats, `sra_summary.rm` and 
`sra_summary.html`.

See `sra_summary.rmd` for more details.