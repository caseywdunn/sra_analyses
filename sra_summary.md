# Metazoan SRA summary

Last updated Mon Jun 15 14:33:49 2015.

## Getting the SRA data

Go to http://www.ncbi.nlm.nih.gov/sra , and make the following query:

    ("strategy rna seq"[Properties] OR transcriptomic[Text Word]  OR cDNA[Text Word] ) AND "platform illumina"[Properties] AND metazoa[Organism] NOT vertebrata[Organism] NOT insects[Organism]


Then click "Send To" and select "File" and "Summary". This will download the file `sra_result.csv`.

Then load the data:

```r
D <- read.csv("sra_result.csv")
```

This gives a total of 6601 samples.


## Filtering the data

There isn't a way to tell from these data if the reads are single ended (SE) or paired end (PE). We can calcualte the the number of bases per spot, which is the read length for SE data and the sum of the forward and reverse read lengths for PE data.

We will filter according to a minimum length that is reasonable for both:


```r
D <- cbind(D, (D$Total.Bases / D$Total.Spots))
names(D)[names(D)=="(D$Total.Bases/D$Total.Spots)"] <- "Spot.Length"

min_length = 70

D <- D[ (D$Spot.Length) >= min_length , ]
```

3989 samples pass the filtering criteria.

## Sumarizing the available data


```r
Ds <- rowsum(D$Total.Spots, group=D$Organism.Name)
Ds
```

```
##                                              [,1]
## Abacion magnum                           31791284
## Abatus agassizii                         56999821
## Abatus cordatus                         186403783
## Abylopsis tetragona                      21575176
## Acanthaster planci                       26482999
## Acanthoscurria geniculata                79370334
## Acoela                                   25440138
## Acropora aspera                         117429587
## Acropora hyacinthus                     220937001
## Adineta ricciae                          11685405
## Adineta vaga                             17607592
## Aegina citrea                            29297167
## Agalma elegans                           53998182
## Agelenopsis emertoni                     24062066
## Agelenopsis pennsylvanica                34898627
## Aiptasia                                633153354
## Aiptasia pallida                        187457932
## Alatina alata                            96259870
## Aliatypus coylei                         29958173
## Alipes grandidieri                       16166017
## Allolobophora chlorotica L1              26522622
## Allolobophora chlorotica L2             130742055
## Allolobophora chlorotica L4              25707653
## Amblyomma americanum                    296473576
## Amblyomma maculatum                      25934570
## Amphibalanus amphitrite                  26806389
## Amphimedon queenslandica                296629720
## Amphiplica gordensis                     86134402
## Amphiura filiformis                     137640419
## Anadara trapezia                         13143087
## Ancylostoma ceylanicum                  198946931
## Androctonus australis                    15028017
## Anguillicola crassus                    213500758
## Anneissia japonica                       23401700
## Antecaridina lauensis                    29578952
## Anthopleura elegantissima               136470063
## Antillesoma antillarum                   45797278
## Antrodiaetus unicolor                    32624239
## Anuroctonus phaiodactylus                 4030956
## Apharyngtus punicus                      31186400
## Aphelenchoides besseyi                    4000000
## Aphelenchoides fragariae                 43364338
## Aphonopelma iviei                        36326210
## Aplysia californica                     659947498
## Aporrectodea icterica                    28354535
## Apostichopus japonicus                  221126444
## Aptostichus atomarius                    27431535
## Aptostichus stephencolberti              30904990
## Arca noae                                47685042
## Architectonica perspectiva               17471149
## Arctica islandica                        51105770
## Argonemertes australiensis               19774213
## Argopecten irradians                     76294890
## Argulus siamensis                        77759443
## Arion vulgaris                          169665303
## Armadillidium nasatum                    15552480
## Armadillidium vulgare                   155679139
## Ascaris suum                                   NA
## Aspidosiphon parvulus                    28256694
## Astarte sulcata                          75343320
## Asterias amurensis                      138554495
## Asterias forbesi                         30844547
## Asterias rubens                          33584666
## Astrotoma agassizii                      27187998
## Asymmetron lucayanum                    161132215
## Atolla vanhoeffeni                        7186161
## Atrina rigida                            21160806
## Aurelia aurita                          162168803
## Austrognathia sp. CEL-2015               20585631
## Azumapecten farreri                     203655180
## Balanoglossus cf. aurantica JTC-2014     26006833
## Barentsia gracilis                       33973668
## Baseodiscus unicolor                     39453222
## Bathydoris clavigera                     12878221
## Bdellocephala annandalei                 28316465
## Bdelloura candida                         4296686
## Belisarius xambeui                       10567924
## Beroe abyssicola                         22722322
## Biomphalaria glabrata                  1252514177
## Bithynia siamensis goniomphalos         105874398
## Bolinopsis infundibulum                  21688585
## Bostrycapulus aculeatus                  11183384
## Bothriomolus balticus                     2783082
## Bothrioplana semperi                     61879582
## Bothriurus burmeisteri                    3133008
## Botryllus schlosseri                   1064734902
## Brachionus calyciflorus                 226935793
## Brachycybe lecontii                      27445580
## Brachythele longitarsis                  30773715
## Branchiostoma belcheri                  613745279
## Branchiostoma floridae                   63500953
## Brotheas granulatus                      20318273
## Brugia malayi                           144394913
## Brugia pahangi                          179886381
## Bugula neritina                         159623975
## Bursaphelenchus mucronatus               16351464
## Bursaphelenchus xylophilus              303228020
## Caenorhabditis afra                      23430085
## Caenorhabditis angaria                   53171790
## Caenorhabditis brenneri                 315985871
## Caenorhabditis briggsae                 585189632
## Caenorhabditis elegans                         NA
## Caenorhabditis japonica                  99876693
## Caenorhabditis nigoni                    30802534
## Caenorhabditis remanei                  761596945
## Caenorhabditis sp. 1 KK-2011             46166144
## Caenorhabditis sp. 10 JR-2014            18797280
## Caenorhabditis tropicalis                31091076
## Caenorhabditis wallacei                  46655333
## Calanus finmarchicus                   1022071388
## Calanus sinicus                          29472239
## Caligus rogercresseyi                  1512096984
## Cambala annulata                         29499905
## Carcinus aestuarii                       78199418
## Cardites antiquatus                      29403785
## Caridina rubella                         30568346
## Carinoma sp. SA-2011                     41644475
## Catenula lemnae                          13240788
## Centruroides exilicauda                  60100128
## Centruroides sculpturatus                24260079
## Centruroides vittatus                    45691843
## Cephalodiscus gracilis                   23873153
## Cephalodiscus hodgsoni                    8591989
## Cephalodiscus nigrescens                 11706583
## Cephalothrix hongkongiensis              26112259
## Cephalothrix linearis                    11041746
## Cerastoderma edule                       17562216
## Cerebratulus marginatus                  26688280
## Cerebratulus sp. BE-2015                 29836780
## Chaerilus celebensis                     20801179
## Chaetoderma sp.                          10131129
## Chaetopterus variopedatus                  302104
## Cherax cainii                            83984583
## Cherax destructor                       100712892
## Cherax quadricarinatus                  139362291
## Chlorostoma funebralis                  450873108
## Cicurina travisae                        29322638
## Cicurina vibora                          29071083
## Ciona intestinalis                      199834460
## Cipangopaludina cathayensis              52990846
## Cleidogona sp. MB-2013                   23567005
## Cliona varians                          103055049
## Clione antarctica                        20282761
## Clonorchis sinensis                     149283598
## Coeloplana                               20842610
## Coenobita                                 6484645
## Coenobita clypeatus                      29987467
## Conus consors                           432355016
## Conus gloriamaris                        51651984
## Conus lenavati                           97157167
## Conus miliaris                          182513765
## Conus tribblei                          153100176
## Cooperia oncophora                       87680263
## Corallium rubrum                        240931152
## Corbicula fluminea                      170777080
## Corticium candelabrum                    76942600
## Craseoa lathetica                        38233199
## Craspedacusta sowerbyi                   61806549
## Crassostrea angulata                     44744802
## Crassostrea corteziensis                 66907295
## Crassostrea gigas                       476038199
## Crassostrea hongkongensis               123739744
## Crassostrea virginica                   409522454
## Craterostigmus tasmanianus               39557979
## Crella elegans                           62931417
## Crepidula fornicata                      39362017
## Crepidula plana                           7414235
## Cryptops hortensis                       26846372
## Cupiennius salei                        214537304
## Cycladicama cumingi                      18444567
## Cyclocosmia truncata                     33664901
## Cydippida                                20634583
## Cyrenoida floridana                      31566664
## Cystodytes dellechiajei                 190329735
## Dactylopodola baltica                     4963997
## Damon variegatus                         64733221
## Daphnia magna                            43906573
## Daphnia pulex                           265087424
## Deinopis longipes                        39222056
## Dendrocoelum lacteum                    878220598
## Dermacentor andersoni                   125839180
## Dermacentor reticulatus                     26563
## Dermacentor variabilis                   60095495
## Dermatophagoides farinae                 31998735
## Dictyocaulus filaria                     14702570
## Dictyocaulus viviparus                  821515897
## Diplocentrus diablo                       4651756
## Diplodonta sp. VG-2014                   16308079
## Dirofilaria immitis                      46392164
## Donacilla cornea                         13777315
## Doris kerguelenensis                     14785164
## Doryteuthis pealeii                     539057385
## Dosidicus gigas                          92469949
## Dugesia japonica                       1095586399
## Dumetocrinus sp. JTC-2014                24740692
## Dysdera crocata                          20060716
## Echinarachnius parma                     41712544
## Echinaster spinulosus                    36853037
## Echinocardium cordatum                  174435410
## Echinocardium mediterraneum             208470700
## Echinococcus                             18566482
## Echinococcus granulosus                 109888130
## Echinococcus multilocularis             893777972
## Echinogammarus veneris                   42628006
## Echinolittorina malaccana                53016896
## Echinoplana celerrima                    41075529
## Ectopleura larynx                        71157984
## Eisenia andrei                           23777582
## Elysia chlorotica                              56
## Elysia cornigera                        231698220
## Elysia timida                           317672681
## Ennucula tenuis                          38724175
## Enoplus brevis                          189380795
## Enteropneusta sp. JTC-2013               22875604
## Ephydatia muelleri                       78257690
## Eremobates sp. PS-2014                   86794767
## Eriocheir sinensis                      371155618
## Ero leonina                              32363996
## Eucidaris tribuloides                    17548074
## Eucrassatella cumingii                   42454880
## Eucyclops serrulatus                     83634701
## Eunicella cavolinii                      11802649
## Euphausia crystallorophias               17536510
## Euplokamis                               34151349
## Eupolybothrus cavernicolus               10794305
## Eurytemora affinis                      624469046
## Euscorpius italicus                      12667128
## Euspira heros                            19157606
## Evechinus chloroticus                   173081672
## Fasciola gigantica                       21874729
## Fasciola hepatica                      1583684591
## Fascioloides magna                       27002004
## Fasciolopsis buski                       35649443
## Fenneropenaeus merguiensis               43801566
## Fiona pinnata                            16450593
## Frontinella communis                     61568323
## Gadila tolmiei                           37971066
## Galba cubensis                           11167820
## Galba truncatula                        123873992
## Galeomma turtoni                         23539466
## Gammarus fossarum                       161865861
## Gasteracantha hasselti                   12564452
## Gemmula speciosa                         58458055
## Geocentrophora applanata                 27740187
## Geocentrophora sphyrocephala             14150229
## Globodera pallida                      1076027987
## Globodera rostochiensis                  60699691
## Glomeridesmus sp. MB-2013                24546123
## Glossus humanus                          15219090
## Glottidia pyramidata                     33806755
## Glycera dibranchiata                     67762006
## Glycera fallax                            3071773
## Glycera tridactyla                       34568519
## Gnathostomula paradoxa                   26481171
## Gnosonesimida sp. IV CEL-2015            45742044
## Golfingia vulgaris                       30624842
## Gorgonia ventalina                      412728484
## Granata imbricata                        30472415
## Habronattus signatus                     75391275
## Habronattus ustulatus                    68825167
## Hadogenes troglodytes                    19210515
## Hadrurus arizonensis                      2906683
## Haemonchus contortus                    971868051
## Haliclona amboinensis                   144947281
## Haliotis midae                            5399167
## Haliotis rufescens                       67755310
## Halocaridinides trigonophthalma          26955642
## Haminoea antillarum                      11999771
## Haplochernes kraepelini                  11976066
## Harmothoe extenuata                       9626876
## Harrimaniidae sp. n. clade 1 JTC-2014    34633208
## Harrimaniidae sp. n. clade 3 JTC-2014    33075786
## Hebestatis theveneti                     40097804
## Heligmosomoides polygyrus                27574147
## Heliocidaris erythrogramma              496817224
## Helix lucorum                            55077351
## Hemithiris psittacea                     30365511
## Henricia sp. AR-2014                     38623931
## Hermodice carunculata                   213277962
## Hesperochernes                           15637772
## Hesperonemastoma modestum                64310060
## Heterodera avenae                        83239544
## Heterololigo bleekeri                   242626243
## Hiatella arctica                         37170745
## Himantarium gabrielis                    28549275
## Hinea brasiliana                          7322512
## Hofstenia miamia                        175089978
## Holothuria glaberrima                   331931211
## Hormiphora californensis                 32337982
## Hormogaster elisae                      104195139
## Hormogaster samnitica                    26978390
## Hottentotta trilineatus                   9498355
## Hubrechtella ijimai                      25423801
## Hyalonema populiferum                    15622728
## Hyas araneus                             98508658
## Hydatina physis                          19252736
## Hydra vulgaris                          139947353
## Hydractinia polyclina                    65294541
## Hydractinia symbiolongicarpus          1815588414
## Hymenolepis microstoma                  415598536
## Hypochilus pococki                       25747925
## Idiops bersebaensis                      23040778
## Itaspiella helgolandica                  28532849
## Iurus dekanum                            11774511
## Ixodes persulcatus                       56380590
## Ixodes ricinus                          369980581
## Ixodes scapularis                      1907154350
## Janthina janthina                        19282072
## Kirkpatrickia variolosa                  14658497
## Kronborgia cf. amphipodicola CEL-2015    41008814
## Kukulcania hibernalis                    42693292
## Labidiaster annulatus                    62315036
## Laevipilina hyalina                      29983919
## Lampsilis cardium                        19317443
## Lamychaena hians                         39018941
## Laqueus californianus                    33707388
## Larifuga capensis                        14653564
## Lasaea adansoni                          17348235
## Latrodectus hesperus                    305315389
## Latrodectus tredecimguttatus             27605467
## Latrunculia apicalis                     12691254
## Lehardyia sp. CEL-2015                    1915781
## Leiobunum verrucosum                     33584952
## Lepadella patella                        29281701
## Lepetodrilus fucensis                    20192994
## Lepidodermella sp. CEL-2015             192956482
## Lepidodermella squamata                  10763661
## Leptasterias sp. AR-2014                 41257296
## Leptochiton rugatus                      24835027
## Leptoplana tremellaris                   38286359
## Leptosynapta clarki                      28011251
## Lernaea cyprinacea                       67324348
## Leucauge venusta                         24650987
## Limacina helicina                        31999474
## Limulus polyphemus                       65099444
## Lineus longissimus                       95576610
## Lineus ruber                             59763351
## Liocarcinus depurator                    65244154
## Liocheles australasiae                    2025401
## Liphistius                               54043289
## Liphistius malayanus                     62897982
## Lithobius forficatus                     28549275
## Lithobius sp. MB-2013                    48208463
## Litopenaeus vannamei                    685679762
## Loa loa                                   5738066
## Longidorus elongatus                     19642943
## Loxosceles reclusa                       76990087
## Loxosoma pectinaricola                   37512776
## Luidia clathrata                         33096844
## Lumbricus terrestris                     47074374
## Lymnaea stagnalis                        81851004
## Lyonsia floridana                        23679875
## Lytechinus variegatus                   857399307
## Macandrevia cranium                       9373527
## Macracantha arcuata                      17523883
## Macracanthorhynchus hirudinaceus        124360449
## Macrobrachium nipponense                103253320
## Macrobrachium rosenbergii               323004618
## Macrochaeta clavicornis                    270271
## Macrodasys sp. THS-2014                  21672520
## Macrostomum cf. ruebushi CEL-2015        20623939
## Macrostomum lignano                      31458283
## Mactra chinensis                         39897194
## Magellania venosa                       210171602
## Magelona berkeleyi                       21449372
## Magelona johnstoni                        4598314
## Malacobdella grossa                      32890242
## Margaritifera margaritifera              15172806
## Maritigrella crozieri                    92140962
## Marphysa bellii                           7529676
## Marthasterias glacialis                  38396211
## Mastigoproctus giganteus                 25983006
## Megacormus sp. PPS-2014                  37103196
## Megahexura fulva                         59599533
## Megalomma vesiculosum                    11537083
## Meloidogyne incognita                    30511449
## Mercenaria campechiensis                 61427408
## Meretrix meretrix                       111676350
## Mertensiidae                             23727123
## Mesodasys laticaudatus                   26906494
## Mesonerilla fagei                        28821483
## Mesostoma lingua                         35266575
## Metabetaeus lohena                       23276200
## Metabetaeus minutus                      25979030
## Metabiantes sp. PS-2014                  72266554
## Metasiro americanus                      12488783
## Micrathena gracilis                      56963267
## Microdalyellia fusca                     27725177
## Microdalyellia schmidti                  34673869
## Microdalyellia sp. CEL-2015               3070375
## Microdipoena guttata                     16972695
## Microhedyle glandulifera                  6194970
## Microhexura montivaga                    52436996
## Microstomum lineare                      46274707
## Mizuhopecten yessoensis                 194045224
## Mnemiopsis leidyi                       103647037
## Molgula                                  73500170
## Molgula occulta                         200781565
## Molgula oculata                         114958249
## Molgula oculata x Molgula occulta        41921840
## Monocelis fusca                          17970185
## Monocelis sp. BE-2015                    26127719
## Monodonta labio                          17130780
## Montastraea faveolata                   458257689
## Mya arenaria                             25979722
## Mycale phyllophila                       23846465
## Myochama anomioides                      77065910
## Mytilus californianus                    38789141
## Mytilus edulis                          261935370
## Mytilus galloprovincialis               351681781
## Mytilus trossulus                        57515221
## Myxobolus cerebralis                    154178467
## Myzostoma cirriferum                      7800476
## Nacobbus aberrans                       193451920
## Nanomia bijuga                          252889563
## Nasoonaria sinensis                      19990955
## Necator americanus                       98645947
## Necora puber                            267196541
## Nematoplana coelogynoporoides            27642780
## Nematostella vectensis                 1523297702
## Neocardia sp. VG-2014                    10719028
## Neocaridina denticulata                  29987459
## Neomenia megatrapezata                   29291588
## Neomeniomorpha sp. 1 SS-2011             35228112
## Neoscona arabesca                        28551664
## Neotrigonia margaritacea                 48123350
## Nephasoma pellucidum                     27193126
## Nephila clavipes                         34853551
## Nephtys caeca                            12687038
## Nerita peloronta                         20333570
## Nesticus bishopi                         50019498
## Nesticus cooperi                         20188741
## Nipponnemertes sp. SA-2014               31177198
## Novocrania anomala                       26121964
## Octopus bimaculoides                    814405374
## Octopus vulgaris                         83402131
## Oecobius cellariorum                     23084006
## Oesophagostomum dentatum                452305211
## Oikopleura dioica                        48538753
## Onchidella floridana                     14872953
## Onchocerca ochengi                      180003618
## Onchocerca volvulus                     468827856
## Oncomelania hupensis                    145008667
## Ophicardelus sulcatus                    16026272
## Ophiocoma echinata                       29802194
## Ophioderma longicauda                    42091309
## Opisthacanthus madagascariensis          21158446
## Opisthorchis viverrini                   39142047
## Ornithodoros rostratus                   26944933
## Ortholasma coronadense                   40372367
## Oscarella carmela                        84543565
## Oscarella sp. SN-2011                    15811352
## Ostertagia ostertagi                     39711617
## Ostrea chilensis                          8101859
## Ostrea edulis                            46487602
## Ostrea stentina                           8266238
## Owenia fusiformis                        27602447
## Oxynoe viridis                           22229095
## Pachylicus acutus                        18681026
## Palaemon carinicauda                     54207796
## Panagrellus redivivus                    45086997
## Panagrolaimus davidi                    143223606
## Pandalus latirostris                    110149263
## Pandinus imperator                       17620229
## Panonychus citri                        143597926
## Panonychus ulmi                         161720981
## Parabuthus transvaalicus                 37673076
## Paragonimus kellicotti                   69874039
## Paragonimus miyazakii                   194208511
## Paralepetopsis sp. CWD-2014               2370313
## Paramphinome jeffreysii                  29727093
## Paranemertes peregrina                   46242211
## Parasteatoda tepidariorum               442080287
## Parastichopus californicus               64225948
## Parastichopus parvimensis                87108446
## Paratropis                               18409810
## Paravaejovis spinigerus                   8892658
## Parborlasia corrugatus                    7637456
## Pardosa pseudoannulata                  490585546
## Patiria miniata                         727527329
## Patiria pectinifera                      68651857
## Pecten maximus                          775752704
## Penaeus monodon                          98174733
## Peripatopsis capensis                    40774042
## Periphylla periphylla                    11763243
## Perna viridis                           301820077
## Petaserpes sp. MB-2013                   34470090
## Petrosia ficiformis                      77406715
## Peucetia longipalpis                     23273514
## Phacoides pectinatus                      7867647
## Phalangium opilio                        16225145
## Phallomedusa solida                      25685273
## Phascolion cryptum                       24962058
## Phascolosoma granulatum                    340498
## Phascolosoma perlucens                   44005999
## Phasianella ventricosa                   26930229
## Philine angasi                           14365288
## Philoponella herediae                    47234871
## Pholcus manueli                          22570061
## Pholcus phalangioides                    24861584
## Phoronis psammophila                     29186091
## Phoronis vancouverensis                  34765518
## Phyllochaetopterus sp. KMH-2014          29623390
## Phylo foetida                             5078277
## Physalia physalis                        36481773
## Physella acuta                           59165180
## Physella gyrina                          14677283
## Pinctada fucata                          35203939
## Pinctada margaritifera                 1062899151
## Pinctada martensii                        6666667
## Pionothele sp. NG-2014                   20155275
## Pisaster ochraceus                       34948776
## Pisaurina mira                            6887470
## Placopecten magellanicus                 15991338
## Planorbarius corneus                     28040804
## Platygyra carnosus                       52308801
## Platynereis dumerilii                   609416727
## Pleurobrachia bachei                    202641360
## Pleurobrachia pileus                     25313211
## Pleurobranchaea californica              18723054
## Pocillopora damicornis                  204274320
## Podocoryna carnea                       665193192
## Poecilosclerida                          13256767
## Polygordius lacteus                      35844553
## Polymesoda caroliniana                   50172505
## Polypodium hydriforme                   106944454
## Pomacea canaliculata                     45702652
## Pomacea diffusa                          24902733
## Pomatoceros lamarckii                    36458077
## Pontastacus leptodactylus               585225436
## Porites australiensis                    70910785
## Portunus trituberculatus                245836791
## Priapulus caudatus                      277289643
## Priapulus cf. caudatus KMK-2014          28665991
## Prionchulus punctatus                    75055464
## Pristionchus pacificus                  197823598
## Procambarus clarkii                     146135036
## Procotyla fluviatilis                   394780007
## Promyrmekiaphila clathrata               25554356
## Proneomenia sp. KMK-2014                 21795477
## Prorhynchus alpinus                      24634465
## Prorhynchus sp. I CEL-2015              104429212
## Prorhynchus stagnalis                    47462361
## Prosogynopora riseri                      2822058
## Prostemmiulus sp. MB-2013                31622533
## Prostheceraeus vittatus                 226262954
## Prosthiostomum siphunculus               28844388
## Prothalotia lehmanni                     26416431
## Protodorvillea kefersteini                9642610
## Protodriloides chaetifer                   346514
## Protodrilus adhaerens                    21824442
## Protolophus singularis                   37494674
## Protomonotresidae sp. CEL-2015           22682337
## Protopelagonemertes beebei               12011532
## Provortex cf. sphagnorum CEL-2015         1255353
## Pseudocellus sp. PS-2014                 52766585
## Pseudodiploria strigosa                 239912175
## Pseudopolydesmus sp. MB-2013             32233544
## Pseudospongosorites suberitoides         19620790
## Ptychodera bahamensis                    20587063
## Ptychodera flava                        605821125
## Pugilina cochlidium                      33772545
## Pyganodon grandis                       100529164
## Pyropelta sp. CWD-2014                   17236041
## Radix balthica                            8461925
## Radix sp. MOTU5                         117794635
## Ramphogordius lacteus                    25452210
## Rhabdopleura sp. 1 JTC-2014               6851916
## Rhabdopleura sp. BE-2015                 35391009
## Rhipicephalus microplus                 261224092
## Rhipicephalus pulchellus                120614564
## Rhipicephalus sanguineus                 71249837
## Rhizoglyphus robini                     154564765
## Rhynchomesostoma rostratum               17821396
## Rhyssoplax olivacea                      23189291
## Ricinoides atewa                         91403481
## Riseriellus occultus                     36140715
## Rissoella caribaea                       20666995
## Rossella fibulata                        20110853
## Rotaria rotatoria                        46904522
## Rotylenchulus reniformis                165979914
## Rubyspira osteovora                      20571106
## Ruditapes decussatus                    171258754
## Ruditapes philippinarum                 101311571
## Sabellaria alveolata                      1539684
## Saccocirrus burchelli                    31486775
## Saccoglossus mereschkowskii              25315486
## Sagmariasus verreauxi                     1079842
## Schistosoma japonicum                    16373387
## Schistosoma mansoni                    1503371269
## Schizocardium cf. brasiliense JTC-2014   20041622
## Schizocosa rovneri                      132349831
## Schizocosmus abatoides                   51773068
## Schmidtea mediterranea                 2135674342
## Sclerodactyla briareus                   32493891
## Scolelepis squamata                       4728159
## Scoloplos armiger                          399266
## Scorpiops sp. PPS-2014                   19929085
## Scutigera coleoptrata                    13325215
## Scylla paramamosain                      52934042
## Scytodes thoracica                       30924460
## Sepia esculenta                          80947907
## Sepioteuthis lessoniana                  67170966
## Sergiolus capulatus                      32765239
## Setaria labiatopapillosa                214398704
## Siphonosoma cumanense                     9831640
## Siro boyerae                             27198018
## Sitalcina lobata                         42614127
## Smeringurus mesaensis                    22861685
## Solemya velum                            33298527
## Solenopharyngidae sp. CEL-2015            3040330
## Sphaerechinus granularis                 43347434
## Sphaerium nucleus                        51015574
## Sphodros rufipes                         51968533
## Spiochaetopterus sp. AW-2014              9851803
## Stegodyphus mimosarum                    39976010
## Steinernema carpocapsae                  54271095
## Steinernema feltiae                      43472350
## Stenostomum leucops                      23562081
## Stenostomum sthenum                      60840086
## Stereobalanus canadensis                 19290646
## Stomolophus meleagris                    54232978
## Strigamia maritima                      100180643
## Strombus gigas                           25099475
## Strongylocentrotus intermedius           79440729
## Strongylocentrotus purpuratus          1361399264
## Strongyloides ratti                     283868744
## Strongyloides stercoralis              1893971589
## Strongyloides venezuelensis             474601429
## Strubellia wawrai                        24132673
## Stylochus ellipticus                     64040688
## Stylophora pistillata                   152552099
## Superstitionia donensis                  23245173
## Sycon ciliatum                          442161433
## Sycon coactum                            78179795
## Syllis sp. AW-2014                         300502
## Sympagella nux                            7555576
## Synsphyronus apimelus                    61948345
## Tachypleus tridentatus                   76305556
## Taenia multiceps                         13723885
## Taenia pisiformis                        13333334
## Tegula atra                              44965610
## Testacella                                     25
## Tetraclita japonica                      26256871
## Tetragnatha kauaiensis                   49857568
## Tetragnatha perreirai                    75484707
## Tetragnatha polychromata                 30345446
## Tetragnatha tantalus                     16474500
## Tetragnatha versicolor                   33465090
## Tetranychus cinnabarinus                 26040173
## Tetranychus urticae                     363224330
## Theba pisana                             51305023
## Thelohanellus kitauei                    49583564
## Theridion californicum                  166104219
## Theridion grallator                     254695482
## Theridion sp. NG-2014                    37459365
## Tigriopus californicus                  227534842
## Titiscania limacina                      24740114
## Tomopteris helgolandica                   6221092
## Torquaratoridae sp. 1 JTC-2014           36862849
## Toxocara canis                          721394185
## Trachelas tranquillus                    15506968
## Trichinella nativa                        5317461
## Trichinella spiralis                     56806548
## Trichopelma laselva                      27264400
## Trichuris muris                         356078709
## Trichuris suis                          313452472
## Trilobodrilus axi                        31642890
## Troglokhammouanus steineri               20456204
## Trogulus martensi                        29387202
## Tubulanus polymorphus                    26189641
## Turbonilla sp. CWD-2014                  26619896
## Turridrupa cerithina                     55352652
## Tylodina fungina                         19608827
## Uloborus glomosus                        12906413
## Unedogemmula bisaya                      47193063
## Uniomerus tetralasmus                   113160338
## Urodacus yaschenkoi                      41906432
## Urosalpinx cinerea                       20644280
## Vallicula multiformis                    24545186
## Varroa destructor                        22920031
## Vietbocap lao                            19987805
## Villosa lienosa                          81583676
## Vonones ornata                           66096788
## Xenoprorhynchus sp. I CEL-2015           15039833
## Yoldia limatula                          38594442
```
686 species have relevant data.


## Selecting focal taxa

The following is a vector of the taxa we wish to consider further:


```r
foci <- c(
"Abylopsis tetragona",
"Agalma elegans"
)
```

Here are the relevant samples:


```r
D[ (D$Organism.Name %in% foci), c( 3, 6, 12, 19 ) ]
```

```
##            Organism.Name Study.Accession Total.Spots Spot.Length
## 5314      Agalma elegans       SRP023468    53998182         200
## 5315 Abylopsis tetragona       SRP023468    21575176         200
```
